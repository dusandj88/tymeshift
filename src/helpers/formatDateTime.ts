import { formatMinutesInDate } from "helpers/formatMinutesInDate";

export const formatDateTime = (date: string) => {
  const formattedDate = new Date(date);

  return `${formattedDate.getUTCHours()}:${formatMinutesInDate(
    formattedDate.getUTCMinutes().toString()
  )}`;
};
