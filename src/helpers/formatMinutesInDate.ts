export const formatMinutesInDate = (minutes: string) => {
  return minutes.length > 1 ? minutes : `0${minutes}`;
};
