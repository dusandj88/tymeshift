import React from "react";
import { Locations } from "components/Locations/Locations";
import { Navigation } from "components/Navigation/Navigation";
import { createTheme, ThemeProvider } from "@mui/material";

const theme = createTheme({
  typography: {
    fontFamily: "Lato",

    allVariants: {
      color: "#001122",
    },
  },
});

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Navigation />
      <Locations />
    </ThemeProvider>
  );
};

export default App;
