import { useState, useEffect } from "react";
import axios from "axios";
import { LocationProps } from "components/Location/Location";

export interface UseFetchProps {
  url: string;
}

export const useFetch = ({ url }: UseFetchProps) => {
  const [data, setData] = useState<LocationProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    setLoading(true);
    setData([]);
    setError(null);
    const source = axios.CancelToken.source();
    axios
      .get<LocationProps[]>(url, { cancelToken: source.token })
      .then((res: { data: LocationProps[] }) => {
        setLoading(false);
        res.data && setData(res.data);
      })
      .catch((error: Error) => {
        if (axios.isAxiosError(error)) setLoading(false);
        setError("An error occurred.");
      });
    return () => {
      source.cancel();
    };
  }, [url]);

  return { data, loading, error };
};
