import styled from "styled-components";
import Button from "@mui/material/Button";

export const StyledButton = styled(Button)`
  background-color: #37b24d !important;
  border-radius: 16px !important;
  color: #ffffff !important;
  font-weight: 600 !important;
  width: 4rem !important;

  &:hover {
    background-color: #37b24d !important;
  }
`;
