import React, { FC } from "react";
import { StyledButton } from "components/Button/styles/StyledButton";

export interface ButtonProps {
  text: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

export const Button: FC<ButtonProps> = ({ text, onClick }) => {
  return <StyledButton onClick={onClick && onClick}>{text}</StyledButton>;
};
