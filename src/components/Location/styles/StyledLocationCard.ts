import styled from "styled-components";
import Card from "@mui/material/Card";

export const StyledLocationCard = styled(Card)`
  width: 21.25rem;
  background-color: #f6f6f620;
  user-select: none;
  cursor: pointer;

  .location__edit-icon {
    display: none;
  }

  &:hover {
    background-color: #f6f6f6;
    .location__edit-icon {
      display: inline-block;
    }
  }
`;
