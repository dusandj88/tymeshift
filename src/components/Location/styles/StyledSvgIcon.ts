import styled from "styled-components";
import SvgIcon from "@mui/material/SvgIcon";

export const StyledSvgIcon = styled(SvgIcon)`
  padding-top: 4px;
`;
