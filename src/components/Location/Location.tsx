import React, { FC, useState } from "react";
import { StyledLocationCard } from "components/Location/styles/StyledLocationCard";
import { StyledSvgIcon } from "components/Location/styles/StyledSvgIcon";
import { LocationModal } from "components/Modal/LocationModal";
import { CardContent, Typography, SvgIcon, Box, Grid } from "@mui/material";
import { ReactComponent as TimezoneSvg } from "assets/icons/Timezone.svg";
import { ReactComponent as ViewsSvg } from "assets/icons/Views.svg";
import { ReactComponent as UsersSvg } from "assets/icons/Users.svg";
import { ReactComponent as EditSvg } from "assets/icons/Edit.svg";
import { formatDateTime } from "helpers/formatDateTime";

export interface LocationProps {
  id: string;
  createdAt: string;
  name: string;
  userCount: string;
  description: string;
}

export const Location: FC<LocationProps> = ({
  id,
  createdAt,
  name,
  userCount,
  description,
}) => {
  const [numberOfViews, setNumberOfViews] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [hovered, setHovered] = useState(false);

  return (
    <>
      <Grid item>
        <StyledLocationCard
          onClick={() => {
            setNumberOfViews(numberOfViews + 1);
            setShowModal(true);
          }}
          onMouseEnter={() => setHovered(true)}
          onMouseLeave={() => setHovered(false)}
        >
          <CardContent style={{ paddingBottom: "1rem" }}>
            <Box display="flex" justifyContent="space-between">
              <Box display="flex">
                <Typography variant="h5" fontWeight="700" fontSize="1.125rem">
                  {name}
                </Typography>
                {hovered && (
                  <Typography variant="h5" fontWeight="700" fontSize="1.125rem">
                    (hover)
                  </Typography>
                )}
              </Box>
              <SvgIcon className="location__edit-icon">
                <EditSvg />
              </SvgIcon>
            </Box>
            <Box component="span" display="flex" marginTop="5px">
              <StyledSvgIcon>
                <UsersSvg />
              </StyledSvgIcon>
              <Typography>{userCount} Users</Typography>
            </Box>
            <Box component="span" display="flex">
              <StyledSvgIcon>
                <TimezoneSvg />
              </StyledSvgIcon>
              <Typography>{formatDateTime(createdAt)}</Typography>
            </Box>
            <Box component="span" display="flex">
              <StyledSvgIcon>
                <ViewsSvg />
              </StyledSvgIcon>
              <Typography>{numberOfViews.toString()} Views</Typography>
            </Box>
          </CardContent>
        </StyledLocationCard>
      </Grid>
      <LocationModal
        showModal={showModal}
        title={name}
        location={{ name, createdAt, description, id, userCount }}
        numberOfViews={numberOfViews}
        setShowModal={setShowModal}
      />
    </>
  );
};
