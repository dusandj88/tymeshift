import { CircularProgress, Alert, AlertTitle } from "@mui/material";
import { Location } from "components/Location/Location";
import React, { FC } from "react";
import { useFetch } from "hooks/useFetch";
import { StyledLocationsGrid } from "components/Locations/styles/StyledLocationsGrid";

export interface LocationsProps {}

export const Locations: FC<LocationsProps> = () => {
  const {
    data: locations,
    loading,
    error,
  } = useFetch({
    url: "https://6033c4d8843b15001793194e.mockapi.io/api/locations",
  });

  if (loading) {
    return <CircularProgress size="5rem" />;
  }

  if (error) {
    return (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        <strong>{error}</strong>{" "}
        <strong>Please contact support for more assistance!</strong>
      </Alert>
    );
  }

  return (
    <>
      <StyledLocationsGrid container spacing={4} style={{ margin: "0 28px" }}>
        {locations.map((location) => (
          <Location
            key={location.id}
            id={location.id}
            name={location.name}
            description={location.description}
            createdAt={location.createdAt}
            userCount={location.userCount}
          />
        ))}
      </StyledLocationsGrid>
    </>
  );
};
