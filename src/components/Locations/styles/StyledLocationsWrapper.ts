import styled from "styled-components";
import { Grid } from "@mui/material";

export const StyledLocationsWrapper = styled(Grid)`
  display: flex;
`;
