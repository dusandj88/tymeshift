import styled from "styled-components";
import Grid from "@mui/material/Grid";

export const StyledLocationsGrid = styled(Grid)`
  justify-content: flex-start;
`;
