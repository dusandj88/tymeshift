import styled from "styled-components";
import Box from "@mui/material/Box";

export const StyledNavigationTitle = styled(Box)`
  color: #001122;
  line-height: 1.75;
  font-size: 1rem;
  font-weight: 700;
  opacity: 0.3;
`;
