import styled from "styled-components";

export const StyledNavigationWrapper = styled.div`
  background-color: #ffffff;
  padding: 1.25rem 1.875rem;
  border-bottom: 1px solid #00000010;
`;
