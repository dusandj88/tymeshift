import styled from "styled-components";
import Box from "@mui/material/Box";

export const StyledNavigationItem = styled(Box)`
  color: #001122;
  line-height: 1.273;
  font-size: 1.375rem;
  font-weight: 600;
  opacity: 0.8;
`;
