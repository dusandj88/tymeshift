import React from "react";
import { StyledNavigationWrapper } from "components/Navigation/styles/StyledNavigationWrapper";
import { StyledNavigationTitle } from "components/Navigation/styles/StyledNavigationTitle";
import { StyledNavigationItem } from "components/Navigation/styles/StyledNavigationItem";

export const Navigation = () => {
  return (
    <StyledNavigationWrapper>
      <StyledNavigationTitle>All locations</StyledNavigationTitle>
      <StyledNavigationItem>Acme locations</StyledNavigationItem>
    </StyledNavigationWrapper>
  );
};
