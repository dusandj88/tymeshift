import React, { FC, Dispatch, SetStateAction } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Grid,
  Typography,
  Box,
  CardContent,
  SvgIcon,
} from "@mui/material";
import { StyledLocationPaper } from "components/Modal/styles/StyledLocationPaper";
import { ReactComponent as TimezoneSvg } from "assets/icons/Timezone.svg";
import { ReactComponent as ViewsSvg } from "assets/icons/Views.svg";
import { ReactComponent as UsersSvg } from "assets/icons/Users.svg";
import { ReactComponent as CloseSvg } from "assets/icons/Close.svg";
import { LocationProps } from "components/Location/Location";
import { formatDateTime } from "helpers/formatDateTime";
import { StyledSvgIcon } from "components/Location/styles/StyledSvgIcon";
import { Button } from "components/Button/Button";

export interface LocationModalProps {
  title: string;
  showModal: boolean;
  location: LocationProps;
  numberOfViews: number;
  setShowModal: Dispatch<SetStateAction<boolean>>;
}

export const LocationModal: FC<LocationModalProps> = ({
  title,
  showModal,
  location: { createdAt, description, userCount },
  numberOfViews,
  setShowModal,
}) => {
  return (
    <Dialog open={showModal} onClose={() => setShowModal(false)}>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <DialogTitle
          style={{ fontSize: "1rem", lineHeight: 1.5, fontWeight: 600 }}
        >
          {title}
        </DialogTitle>
        <SvgIcon
          style={{
            paddingRight: "0.75rem",
            opacity: "0.3",
            cursor: "pointer",
            paddingTop: "4px",
          }}
        >
          <CloseSvg onClick={() => setShowModal(false)} />
        </SvgIcon>
      </Box>
      <DialogContent>
        <Grid item md={2}>
          <StyledLocationPaper>
            <CardContent style={{ backgroundColor: "white", padding: 0 }}>
              <Box component="span" display="flex">
                <StyledSvgIcon opacity="0.3">
                  <UsersSvg />
                </StyledSvgIcon>
                <Typography fontWeight="500">{userCount} Users</Typography>
              </Box>
              <Box component="span" display="flex">
                <StyledSvgIcon opacity="0.3">
                  <TimezoneSvg />
                </StyledSvgIcon>
                <Typography fontWeight="500">
                  {formatDateTime(createdAt)}
                </Typography>
              </Box>
              <Box component="span" display="flex">
                <StyledSvgIcon opacity="0.3">
                  <ViewsSvg />
                </StyledSvgIcon>
                <Typography fontWeight="500">
                  {numberOfViews.toString()} Views
                </Typography>
              </Box>
              <Typography fontWeight="600" marginTop="1.25rem">
                Description
              </Typography>
              <Typography>{description}</Typography>
            </CardContent>
          </StyledLocationPaper>
        </Grid>
        <Box style={{ float: "right", marginTop: "1rem" }}>
          <Button text="Done" onClick={() => setShowModal(false)} />
        </Box>
      </DialogContent>
    </Dialog>
  );
};
