import styled from "styled-components";

export const StyledLocationPaper = styled.div`
  width: 21.25rem;
  background-color: #f6f6f6;
  user-select: none;
  cursor: pointer;

  .location__edit-icon {
    display: none;
  }

  &:hover {
    .location__edit-icon {
      display: inline-block;
    }
  }
`;
